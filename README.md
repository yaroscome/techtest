# Description #
- This project uses Google-style MVP architecture with Dagger 2 DI and Dagger 2 AndroidInjection
- As by requirements for production code, the App uses only libraries listed in the spec
- For unit test I've used Mockito, Fixture and Mockito-Kotlin (minimum necessary libraries)
- For simplicity, database/caching was omitted.

### api package ####
- `ApiInteractor` communicates to the server via ApiService. In this particular case it simply defers the calls to the presenter. In other cases it could have more Rx operators chained to deserialize, filter, schedule or process data in any other way.
- `DTOs.kt` contains all necessary data classes used for API communication.
- `ApiModule` Dagger 2 module fro API communication.

### common package ###
Contains all necessary classes used across the app: `App` class that extends `Application`, base DI component and main DI module, resource and schedule wrappers.

### home package ###
- Home package contains `HomeActiviy` with contract and related presenter
- Mapper classes that transform data from DTO (network layer) to domain layer (business logic layer). In this case business logic data is same as presentation layer data. In other cases it might have an additional mapping layer.
- `HomePresenter` issues 1st call on initialization and downloads all the necessary data for the RecyclerView or will issue an error in case the date is not available. If the app is paused while an API call is in progress, it will clear the call. For simplicity I have omitted the case when the app gets back in foreground after disposed call.

### detail package ###
- Detail package contains `DetailActivity` that is called by ClickListener initialized in the `ArticleAdapter`. Considering that ArticleModel is very simple data class, it was marked as Serializable and passed entirely to Detail activity. In production app, it could be passed as Serializable, Parcelable, or just a single field depending on business needs and real performance impact.

## Coupling ##
Software components can be coupled via Interactors and mappers. Interactors are effectivly classes that issue API calls or any other action with data providers (DB, files, etc). Mappers instead transform data from DTO/network level data to business data filtering out unnecessary fields or re-shaping data as needed by business logic.

Alternatively, for larger size app, all software components could be broken down into gradle modules.

## Unit Tests ##
For demo purposes, there are 2 classes covered by unit tests: `HomePresenter` and `ArticleDtoToModelMapper`. They cover all existing functionality.

## Improvements ##
- The app could have data caching via ROOM/DB or similar to optimize network calls.
- UI tests would be useful for RecyclerView and navigation between activities.
- Full coverage with unit tests.