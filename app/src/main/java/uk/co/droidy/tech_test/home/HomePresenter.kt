package uk.co.droidy.tech_test.home

import android.annotation.SuppressLint
import io.reactivex.disposables.CompositeDisposable
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.api.ApiInteractor
import uk.co.droidy.tech_test.common.extensions.addTo
import uk.co.droidy.tech_test.common.resources.StringResource
import uk.co.droidy.tech_test.common.scheduler.AppScheduler
import uk.co.droidy.tech_test.home.article_list.ArticleDtoToModelMapper
import uk.co.droidy.tech_test.home.article_list.ArticleModel
import javax.inject.Inject

class HomePresenter @Inject constructor(
    private val view: HomeContract.View,
    private val apiInteractor: ApiInteractor,
    private val articleDtoToModelMapper: ArticleDtoToModelMapper,
    private val stringResource: StringResource,
    private val compositeDisposable: CompositeDisposable,
    private val scheduler: AppScheduler
) : HomeContract.Presenter {

    @SuppressLint("CheckResult")
    override fun init() {

        apiInteractor.getArticles()
            .subscribeOn(scheduler.io())
            .map(articleDtoToModelMapper)
            .observeOn(scheduler.mainThread())
            .subscribe({
                view.initView(it)
            }, {
                val defaultError = stringResource.getStringFromId(R.string.global_default_error_message)
                //TODO could use an error mapper or receive proper error messages from Server
                view.showError(it.message ?: defaultError)
            })
            .addTo(compositeDisposable)
    }

    override fun onPause() {
        compositeDisposable.clear()
    }
}