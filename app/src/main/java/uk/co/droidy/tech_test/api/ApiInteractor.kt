package uk.co.droidy.tech_test.api

import io.reactivex.Single
import uk.co.droidy.tech_test.api.di.ApiService
import uk.co.droidy.tech_test.api.dto.ItemContainerDTO
import uk.co.droidy.tech_test.api.dto.ItemsDTO
import uk.co.droidy.tech_test.common.scheduler.AppScheduler
import javax.inject.Inject

class ApiInteractor
@Inject constructor(private val apiService: ApiService) {

    fun getArticles(): Single<ItemsDTO> {
        return Single.defer {
            return@defer apiService.getArticles()
        }
    }

    fun getArticleById(id: String): Single<ItemContainerDTO> {
        return Single.defer {
            return@defer apiService.getItem(id)
        }
    }
}