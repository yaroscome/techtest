package uk.co.droidy.tech_test.common.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import uk.co.droidy.tech_test.common.App
import uk.co.droidy.tech_test.api.di.ApiModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class])
interface AppComponent: AndroidInjector<App> {
    override fun inject(application: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(application: App): Builder

        fun build(): AppComponent
    }

}