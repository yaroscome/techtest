package uk.co.droidy.tech_test.common.resources

import android.content.Context
import android.support.annotation.StringRes
import javax.inject.Inject

class StringResourceWrapper @Inject constructor(private val context: Context): StringResource {

    override fun getStringFromId(@StringRes stringId: Int): String {
        return context.getString(stringId)
    }

    override fun getStringFromId(@StringRes stringId: Int, formatArgs: Any): String {
        return context.getString(stringId, formatArgs)
    }

}