package uk.co.droidy.tech_test.home.di

import dagger.Binds
import dagger.Module
import uk.co.droidy.tech_test.home.HomeContract
import uk.co.droidy.tech_test.home.HomePresenter

@Suppress("unused")
@Module(includes = [HomeModule.HomeBindings::class])
class HomeModule {

    @Module
    interface HomeBindings {

        @Binds
        fun bindHomePresenter(homePresenter: HomePresenter): HomeContract.Presenter
    }
}
