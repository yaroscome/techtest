package uk.co.droidy.tech_test.detail

import uk.co.droidy.tech_test.home.article_list.ArticleModel

interface DetailContract {
    interface View {
        fun setData(itemModel: ItemModel)
        fun showError(message: String)
    }

    interface Presenter {
        fun setData(articleModel: ArticleModel)
        fun onPause()
    }
}