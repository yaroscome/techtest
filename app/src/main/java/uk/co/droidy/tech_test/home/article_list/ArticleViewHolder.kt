package uk.co.droidy.tech_test.home.article_list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import uk.co.droidy.tech_test.R

class ArticleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val title: TextView = itemView.findViewById(R.id.title_tv)

    fun bind(model: ArticleModel, listener: View.OnClickListener?) {
        title.text = model.title

        itemView.setOnClickListener(listener)
    }

}