package uk.co.droidy.tech_test.detail

data class ItemModel(
    val title: String,
    val body: String,
    val date: String
)