package uk.co.droidy.tech_test.api.dto

data class ItemsDTO(
    val items: List<ArticleDTO>
)

data class ArticleDTO(
    val id: Int,
    val title: String,
    val subtitle: String,
    val date: String
)

data class ItemContainerDTO(
    val item: ItemDTO
)

data class ItemDTO(
    val id: Int,
    val title: String,
    val subtitle: String,
    val body: String,
    val date: String
)