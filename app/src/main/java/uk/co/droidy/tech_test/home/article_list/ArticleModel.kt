package uk.co.droidy.tech_test.home.article_list

import java.io.Serializable

data class ArticleModel(
    val id: Int,
    val title: String,
//    val subtitle: String,
    val date: String
) : Serializable
