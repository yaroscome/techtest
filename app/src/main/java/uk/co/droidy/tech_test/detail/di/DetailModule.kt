package uk.co.droidy.tech_test.detail.di

import dagger.Binds
import dagger.Module
import uk.co.droidy.tech_test.detail.DetailContract
import uk.co.droidy.tech_test.detail.DetailPresenter

@Suppress("unused")
@Module
interface DetailModule {

    @Binds
    fun bindHomePresenter(detailPresenter: DetailPresenter): DetailContract.Presenter

}