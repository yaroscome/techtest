package uk.co.droidy.tech_test.common.extensions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.addTo(container: CompositeDisposable) {
    container.add(this)
}