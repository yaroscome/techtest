package uk.co.droidy.tech_test.common.scheduler

import io.reactivex.Scheduler

interface AppScheduler {
    fun io(): Scheduler
    fun mainThread(): Scheduler
}