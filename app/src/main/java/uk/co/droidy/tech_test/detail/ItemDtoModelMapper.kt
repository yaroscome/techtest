package uk.co.droidy.tech_test.detail

import io.reactivex.functions.Function
import uk.co.droidy.tech_test.api.dto.ItemContainerDTO
import javax.inject.Inject

class ItemDtoModelMapper
@Inject constructor() : Function<ItemContainerDTO, ItemModel> {
    override fun apply(t: ItemContainerDTO): ItemModel {
        return ItemModel(t.item.title, t.item.body, t.item.date)
    }
}