package uk.co.droidy.tech_test.api.di

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import uk.co.droidy.tech_test.api.dto.ItemContainerDTO
import uk.co.droidy.tech_test.api.dto.ItemsDTO

interface ApiService {

    @GET("/test/native/contentList.json")
    fun getArticles(): Single<ItemsDTO>

    @GET("/test/native/content/{id}.json")
    fun getItem(
        @Path(value = "id") id: String
    ): Single<ItemContainerDTO>

}