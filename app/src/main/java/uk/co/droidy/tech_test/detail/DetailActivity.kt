package uk.co.droidy.tech_test.detail

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_detail.*
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.home.article_list.ArticleModel
import javax.inject.Inject

class DetailActivity : AppCompatActivity(), DetailContract.View {

    @Inject lateinit var presenter: DetailContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val postModel = intent.getSerializableExtra(ITEM) as ArticleModel
        presenter.setData(postModel)
    }

    override fun setData(itemModel: ItemModel) {
        detail_title_tv.text = itemModel.title
        detail_body_tv.text = itemModel.body
        detail_date_tv.text = itemModel.date
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        AlertDialog.Builder(this)
                .setTitle(R.string.global_default_error_heading)
                .setMessage(message)
                .setPositiveButton(R.string.global_ok_uppercase, null)
                .create()
                .show()
    }

    companion object {
        const val ITEM = "ITEM"
    }
}
