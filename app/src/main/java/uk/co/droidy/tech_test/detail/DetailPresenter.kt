package uk.co.droidy.tech_test.detail

import android.annotation.SuppressLint
import io.reactivex.disposables.CompositeDisposable
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.api.ApiInteractor
import uk.co.droidy.tech_test.common.extensions.addTo
import uk.co.droidy.tech_test.common.resources.StringResource
import uk.co.droidy.tech_test.common.scheduler.AppScheduler
import uk.co.droidy.tech_test.home.article_list.ArticleModel
import javax.inject.Inject

class DetailPresenter
@Inject constructor(
    private val view: DetailContract.View,
    private val apiInteractor: ApiInteractor,
    private val itemDtoModelMapper: ItemDtoModelMapper,
    private val stringResource: StringResource,
    private val compositeDisposable: CompositeDisposable,
    private val scheduler: AppScheduler
) : DetailContract.Presenter {

    @SuppressLint("CheckResult")
    override fun setData(articleModel: ArticleModel) {

        apiInteractor.getArticleById(articleModel.id.toString())
            .subscribeOn(scheduler.io())
            .map(itemDtoModelMapper)
            .observeOn(scheduler.mainThread())
            .subscribe({
                view.setData(it)
            }, {
                val defaultError = stringResource.getStringFromId(R.string.global_default_error_message)
                //TODO could use an error mapper or receive proper error messages from Server
                view.showError(it.message ?: defaultError)
            })
            .addTo(compositeDisposable)
    }

    override fun onPause() {
        compositeDisposable.clear()
    }
}