package uk.co.droidy.tech_test.home

import uk.co.droidy.tech_test.home.article_list.ArticleModel

interface HomeContract {
    interface View {
        fun initView(data: List<ArticleModel>)
        fun showError(message: String)
    }

    interface Presenter {
        fun init()
        fun onPause()
    }
}