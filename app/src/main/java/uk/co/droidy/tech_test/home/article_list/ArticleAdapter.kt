package uk.co.droidy.tech_test.home.article_list

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.detail.DetailActivity
import javax.inject.Inject

class ArticleAdapter
@Inject constructor(): RecyclerView.Adapter<ArticleViewHolder>() {

    private var dataList: List<ArticleModel> = ArrayList()

    fun setData(data: List<ArticleModel>) {
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.article_item, parent, false)

        return ArticleViewHolder(view)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val listener = View.OnClickListener {
            val intent = Intent(it.context, DetailActivity::class.java)
            intent.putExtra(DetailActivity.ITEM, dataList[position])
            it.context.startActivity(intent)
        }

        holder.bind(dataList[position], listener)
    }

    override fun getItemCount(): Int = dataList.size

}