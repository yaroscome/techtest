package uk.co.droidy.tech_test.home.article_list

import io.reactivex.functions.Function
import uk.co.droidy.tech_test.api.dto.ItemsDTO
import javax.inject.Inject

class ArticleDtoToModelMapper
@Inject constructor() : Function<ItemsDTO, List<ArticleModel>> {
    override fun apply(t: ItemsDTO): List<ArticleModel> {
        val articleModelList = ArrayList<ArticleModel>()

        for (articleDTO in t.items) {
            articleModelList.add(ArticleModel(articleDTO.id, articleDTO.title, articleDTO.date))
        }

        return articleModelList
    }

}