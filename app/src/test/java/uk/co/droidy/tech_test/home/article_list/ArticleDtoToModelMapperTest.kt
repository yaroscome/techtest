package uk.co.droidy.tech_test.home.article_list

import com.flextrade.jfixture.FixtureAnnotations
import com.flextrade.jfixture.annotations.Fixture
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import uk.co.droidy.tech_test.api.dto.ItemsDTO

class ArticleDtoToModelMapperTest() {
    @Fixture lateinit var fixtItemsDTO: ItemsDTO

    private lateinit var sut: ArticleDtoToModelMapper

    @Before
    fun setUp() {
        FixtureAnnotations.initFixtures(this)

        sut = ArticleDtoToModelMapper()
    }

    @Test
    fun apply() {

        val actualResult = sut.apply(fixtItemsDTO)

        for ((i, value) in fixtItemsDTO.items.withIndex()) {
            assertEquals(actualResult[i].id, value.id)
            assertEquals(actualResult[i].title, value.title)
            assertEquals(actualResult[i].date, value.date)
        }
    }
}