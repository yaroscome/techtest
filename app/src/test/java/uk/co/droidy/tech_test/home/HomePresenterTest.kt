package uk.co.droidy.tech_test.home

import com.flextrade.jfixture.FixtureAnnotations
import com.flextrade.jfixture.annotations.Fixture
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import uk.co.droidy.tech_test.R
import uk.co.droidy.tech_test.api.ApiInteractor
import uk.co.droidy.tech_test.api.dto.ItemsDTO
import uk.co.droidy.tech_test.common.resources.StringResource
import uk.co.droidy.tech_test.home.article_list.ArticleDtoToModelMapper
import uk.co.droidy.tech_test.home.article_list.ArticleModel
import uk.co.droidy.tech_test.test_utils.TestAppScheduler

const val ANY_DEFAULT_ERROR_STRING = "ANY_DEFAULT_ERROR_STRING"
const val ANY_SERVER_ERROR_MESSAGE = "ANY_SERVER_ERROR_MESSAGE"

class HomePresenterTest {
    private val scheduler = TestAppScheduler()

    @Fixture lateinit var fixtItemsDTO: ItemsDTO
    @Fixture lateinit var fixtArticleList: List<ArticleModel>

    @Mock lateinit var mockView: HomeContract.View
    @Mock lateinit var mockApiInteractor: ApiInteractor
    @Mock lateinit var mockArticleDtoToModelMapper: ArticleDtoToModelMapper
    @Mock lateinit var mockStringResource: StringResource
    @Mock lateinit var mockCompositeDisposable: CompositeDisposable

    private lateinit var sut: HomePresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        FixtureAnnotations.initFixtures(this)

        sut = HomePresenter(mockView, mockApiInteractor, mockArticleDtoToModelMapper, mockStringResource, mockCompositeDisposable, scheduler)
    }

    @Test
    fun init_success() {
        //setup
        whenever(mockApiInteractor.getArticles()).thenReturn(Single.just(fixtItemsDTO))
        whenever(mockArticleDtoToModelMapper.apply(fixtItemsDTO)).thenReturn(fixtArticleList)

        //run
        sut.init()

        //verify
        verify(mockApiInteractor).getArticles()
        verify(mockView).initView(fixtArticleList)
    }

    @Test
    fun init_showDefaultErrorMessage() {
        //setup
        whenever(mockStringResource.getStringFromId(R.string.global_default_error_message)).thenReturn(ANY_DEFAULT_ERROR_STRING)
        whenever(mockApiInteractor.getArticles()).thenReturn(Single.error(Throwable()))

        //run
        sut.init()

        //verify
        verify(mockView).showError(ANY_DEFAULT_ERROR_STRING)
    }

    @Test
    fun init_showServerErrorMessage() {
        //setup
        whenever(mockApiInteractor.getArticles()).thenReturn(Single.error(Throwable(ANY_SERVER_ERROR_MESSAGE)))

        //run
        sut.init()

        //verify
        verify(mockView).showError(ANY_SERVER_ERROR_MESSAGE)
    }

    @Test
    fun onPause_disposeCalls() {
        //setup
        whenever(mockApiInteractor.getArticles()).thenReturn(Single.just(fixtItemsDTO))
        whenever(mockArticleDtoToModelMapper.apply(fixtItemsDTO)).thenReturn(fixtArticleList)
        sut.init()

        //run
        sut.onPause()

        //verify
        verify(mockCompositeDisposable).clear()
    }
}